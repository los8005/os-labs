#include "map.h"

Map *CreateMap()
{
	HANDLE hHeap = HeapCreate(0,sizeof(Map),0);
	Map *A = (Map *)HeapAlloc(hHeap,HEAP_ZERO_MEMORY,sizeof(Map));
	A->hHeap = hHeap;
	return A;
}

bool DestroyMap(Map *A)
{
	return HeapDestroy(A->hHeap);
}

void SetValue(Map *A,int key,char *value)
{
	DWORD size = HeapSize(A->hHeap,0,A);
	A[(size/sizeof(Map))-1].key = key;
	strcpy(A[(size/sizeof(Map))-1].value,value);
	A[(size/sizeof(Map))-1].hHeap = A->hHeap;
	A = (Map *)HeapReAlloc(A->hHeap,HEAP_ZERO_MEMORY,A,size + sizeof(Map));
}

bool DelValue(Map *A,int key)
{
	DWORD size = HeapSize(A->hHeap,0,A);

	if(size == sizeof(Map))
		return false;

	for(int i = 0;i<(size/sizeof(Map))-1;i++)
	{
		if(A[i].key == key)
		{
			A[i].key = A[(size/sizeof(Map))-2].key;
			strcpy(A[i].value,A[size/sizeof(Map)-2].value);
			A[i].hHeap = A[size/sizeof(Map)-2].hHeap;

			HANDLE hHeap = A[size/sizeof(Map)-2].hHeap;
			memset(&A[size/sizeof(Map)-2],0,sizeof(Map));
			A[size/sizeof(Map)-2].hHeap = hHeap;

			A = (Map *)HeapReAlloc(A->hHeap,0,A,size - sizeof(Map));
			return true;
		}
	}

	return false;
}

char *GetValue(Map *A,int key)
{
	DWORD size = HeapSize(A->hHeap,0,A);
	for(int i = 0;i<(size/sizeof(Map))-1;i++)
		if(A[i].key == key)
			return A[i].value;
	return NULL;
}

void SaveMap(Map *A,char *szFile)
{
	DWORD size = HeapSize(A->hHeap,0,A);

	DWORD ioBuf;
	HANDLE hFile = CreateFileA(szFile,GENERIC_WRITE,NULL,NULL,CREATE_ALWAYS,NULL,NULL);
	WriteFile(hFile,&size,sizeof(DWORD),&ioBuf,NULL);
	for(int i = 0;i<(size/sizeof(Map))-1;i++)
		WriteFile(hFile,&A[i],sizeof(Map),&ioBuf,NULL);
	CloseHandle(hFile);
}

Map *LoadMap(char *szFile)
{
	Map *A = CreateMap();

	DWORD size;

	DWORD ioBuf;
	HANDLE hFile = CreateFileA(szFile,GENERIC_READ,NULL,NULL,OPEN_EXISTING,NULL,NULL);
	ReadFile(hFile,&size,sizeof(DWORD),&ioBuf,NULL);

	for(int i = 0;i<(size/sizeof(Map))-1;i++)
	{
		Map B;
		ReadFile(hFile,&B,sizeof(Map),&ioBuf,NULL);
		SetValue(A,B.key,B.value);
	}

	CloseHandle(hFile);

	return A;
}