#pragma once

#include <Windows.h>

struct Map
{
	int key;
	char value[10];
	HANDLE hHeap;
};

Map *CreateMap();
bool DestroyMap(Map *A);
void SetValue(Map *A,int key,char *value);
bool DelValue(Map *A,int key);
char *GetValue(Map *A,int key);

void SaveMap(Map *A,char *szFile);
Map *LoadMap(char *szFile);